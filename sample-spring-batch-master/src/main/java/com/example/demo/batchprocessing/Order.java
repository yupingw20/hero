package com.example.demo.batchprocessing;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// @Entity ：表明是一个实体类
@Entity

//声明此对象映射到数据库的数据表，通过它可以为实体指定表(talbe)
//其实表和类名是不一样的，所以加上@Table (name = "orders")，这样与类关联的表就可以被框架识别了。
@Table(name="orders")

//このクラスはcsvの構成、テーブルの構成を表現します
public class Order {
	//@Id 标注用于声明一个实体类的属性映射为数据库的主键列
	@Id
	//@GeneratedValue 用于标注主键的生成策略，通过strategy 属性指定
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(Integer itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	private Integer customerId;

	private Integer itemId;

	private String itemName;

	private Integer itemPrice;

	private String purchaseDate;
}

