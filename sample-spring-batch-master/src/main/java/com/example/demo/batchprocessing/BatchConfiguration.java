package com.example.demo.batchprocessing;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
//@EnableBatchProcessing让我们可以运行Spring Batch
//spring 会自动 帮我们生成一系列与spring batch 运行有关的bean，
//并交给spring容器管理，而当我们需要这些beans时，只需要用一个@autowired就可以实现注入了
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	private OrderRepository orderRepository;

	@Bean
	//通常情况下，可以选择使用多个现有类从DB导入、从文件导入、从csv导入，因此选择适用于它的类的FlatFileItemReader。
	//还要指定csv的位置是“orders.csv”
//	用names()函数指定列名
//	指定将读取的 csv 行存储在 Order 类中
	public ItemReader<Order> reader() {
		return new FlatFileItemReaderBuilder<Order>()
			.name("orderItemReader")
			.resource(new ClassPathResource("orders.csv"))
			.delimited()
			.names(new String[] {"CustomerId", "ItemId", "ItemPrice", "ItemName", "PurchaseDate"})
			.fieldSetMapper(new BeanWrapperFieldSetMapper<Order>() {{
				setTargetType(Order.class);
			}})
			.build();
	}

	@Bean
	//处理从csv读取的数据。这次什么都不处理，直接交给wirter，但实际上，可能会写入验证和转换等处理。
	public ItemProcessor<Order, Order> processor() {
		return new ItemProcessor<Order, Order>() {

			@Override
			public Order process(final Order order) throws Exception {
				return order;
			}
		};
	}


	@Bean
	//从ItemProcessor接收处理后的数据并对其进行处理以便将其导入DB
	public ItemWriter<Order> writer() {
		RepositoryItemWriter<Order> writer = new RepositoryItemWriter<>();
		writer.setRepository(orderRepository);
		writer.setMethodName("save");
		return writer;
	}


	@Bean
//	批处理中的Job之一。一个实际的batch有多个Job是很常见的，
//	但是由于这个程序比较简单，所以只存在这个Job。用于捕获 csv 的作业。
//设置监听器一直监听Job执行完成的事件，如果有就设定listener：listener(new JobCompletionNotificationListener())
	//调用step1的执行
	public Job importOrderJob()
	{
		return jobBuilderFactory.get("importOrderJob")
			.incrementer(new RunIdIncrementer())
			.listener(new JobCompletionNotificationListener())
			.flow(step1())
			.end()
			.build();
	}

	@Bean
	//importOrderJobの最初、一つのみの実行ステップです。
//	声明块一次处理 10 个
//	声明reader()、processor()、writer()依次执行
	public Step step1() {
	  return stepBuilderFactory.get("step1")
	    .<Order, Order> chunk(10)
	    .reader(reader())
	    .processor(processor())
	    .writer(writer())
	    .build();
	}
}
