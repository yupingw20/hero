create table student(
Sid varchar(20),
Sname varchar(20),
Sage date,
Ssex varchar(20)
)


insert into Student values('01' , '赵雷' , '1990-01-01' , '男');
insert into Student values('02' , '钱电' , '1990-12-21' , '男');
insert into Student values('03' , '孙风' , '1990-12-20' , '男');
insert into Student values('04' , '李云' , '1990-12-06' , '男');
insert into Student values('05' , '周梅' , '1991-12-01' , '女');
insert into Student values('06' , '吴兰' , '1992-01-01' , '女');
insert into Student values('07' , '郑竹' , '1989-01-01' , '女');
insert into Student values('09' , '张三' , '2017-12-20' , '女');
insert into Student values('10' , '李四' , '2017-12-25' , '女');
insert into Student values('11' , '李四' , '2012-06-06' , '女');
insert into Student values('12' , '赵六' , '2013-06-13' , '女');
insert into Student values('13' , '孙七' , '2014-06-01' , '女');



create table course(
cid varchar(20),
cname varchar(20),
tid varchar(20))


insert into Course values('01' , '语文' , '02');
insert into Course values('02' , '数学' , '01');
insert into Course values('03' , '英语' , '03');




create table Teacher(TId varchar(10),Tname varchar(10))


insert into Teacher values('01' , '张三');
insert into Teacher values('02' , '李四');
insert into Teacher values('03' , '王五');



create table SC(SId varchar(10),CId varchar(10),score int)

insert into SC values('01' , '01' , 80);
insert into SC values('01' , '02' , 90);
insert into SC values('01' , '03' , 99);
insert into SC values('02' , '01' , 70);
insert into SC values('02' , '02' , 60);
insert into SC values('02' , '03' , 80);
insert into SC values('03' , '01' , 80);
insert into SC values('03' , '02' , 80);
insert into SC values('03' , '03' , 80);
insert into SC values('04' , '01' , 50);
insert into SC values('04' , '02' , 30);
insert into SC values('04' , '03' , 20);
insert into SC values('05' , '01' , 76);
insert into SC values('05' , '02' , 87);
insert into SC values('06' , '01' , 31);
insert into SC values('06' , '03' , 34);
insert into SC values('07' , '02' , 89);
insert into SC values('07' , '03' , 98);


--8查询「李」姓老师的数量
select count(tname) from teacher
where tname like '李%'


--9查询学过「张三」老师授课的同学的信息
select t.tname,c.cid
from teacher as t
inner join course as c
on t.tid = c.tid

select sc.sid,x.tname
from sc
inner join (select t.tname,c.cid
from teacher as t
inner join course as c
on t.tid = c.tid
)as x
on sc.cid =x.cid

select s.sname,s.sage,s.ssex,y.tname
from student as s
inner join (select sc.sid,x.tname
from sc
inner join (select t.tname,c.cid
from teacher as t
inner join course as c
on t.tid = c.tid
)as x
on sc.cid =x.cid)as y
on s.sid =y.sid
where y.tname = '张三'



--10查询没有学全所有课程的同学的信息
select sc.sid,sc.cid,c.cname
from sc
inner join course as c
on sc.cid = c.cid


select s.sid,s.sname,s.sage,s.ssex,a.cname,a.cid
from student as s
inner join (select sc.sid,sc.cid,c.cname
from sc
inner join course as c
on sc.cid = c.cid
)as a
on s.sid = a.sid


select count(sname),sname
from (select s.sid,s.sname,s.sage,s.ssex,a.cname,a.cid
from student as s
inner join (select sc.sid,sc.cid,c.cname
from sc
inner join course as c
on sc.cid = c.cid
)as a
on s.sid = a.sid)as b
group by sname
having count(sname)<3


--11查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
--学号为01的学生
select sid,sname
from student as s
where sid = '01'
--找到cid
select a.sid,a.sname,sc.cid
from (select sid,sname
from student as s
where sid = '01'
)as a
inner join sc
on a.sid = sc.sid


--sc中包含010203的
select  sc.sid,sc.cid
from sc
inner join (select a.sid,a.sname,sc.cid
from (select sid,sname
from student as s
where sid = '01'
)as a
inner join sc
on a.sid = sc.sid)as a
on sc.cid = a.cid


select distinct s.sname,s.sage,s.ssex
from student as s
inner join (select  sc.sid,sc.cid
from sc
inner join (select a.sid,a.sname,sc.cid
from (select sid,sname
from student as s
where sid = '01'
)as a
inner join sc
on a.sid = sc.sid)as a
on sc.cid = a.cid)as b
on s.sid= b.sid



--12查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息

select sc.sid,x.cid
from (select a.sid,a.sname,sc.cid
from (select sid,sname
from student as s
where sid = '01'
)as a
left join sc
on a.sid = sc.sid
)as x
inner join sc
on x.cid = sc.cid


select distinct y.sid
from(select sc.sid,x.cid
from (select a.sid,a.sname,sc.cid
from (select sid,sname
from student as s
where sid = '01'
)as a
left join sc
on a.sid = sc.sid
)as x
inner join sc
on x.cid = sc.cid
)as y
group by sid
having count(cid)=3

select s.sid,s.sname,s.sage,s.ssex
from (select distinct y.sid
from(select sc.sid,x.cid
from (select a.sid,a.sname,sc.cid
from (select sid,sname
from student as s
where sid = '01'
)as a
left join sc
on a.sid = sc.sid
)as x
inner join sc
on x.cid = sc.cid
)as y
group by sid
having count(cid)=3)as a
left join student as s
on a.sid =s.sid

--13查询没学过"张三"老师讲授的任一门课程的学生姓名

select distinct sc.sid
from sc
inner join course as c
on sc.cid =c.cid
inner join teacher as t
on c.tid=t.tid
where  Tname = '张三'

select  sname
from student 
where sid not in (select distinct sc.sid
from sc
inner join course as c
on sc.cid =c.cid
inner join teacher as t
on c.tid=t.tid
where  Tname = '张三')

