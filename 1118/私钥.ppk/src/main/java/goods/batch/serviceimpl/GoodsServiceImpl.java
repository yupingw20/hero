package goods.batch.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import goods.batch.entity.GoodsListEntity;
import goods.batch.mapper.GoodsMapper;
import goods.batch.service.GoodsService;

@Service
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	private GoodsMapper goodsMapper;

	/**
	 * @return int insertした件数
	 */
	@Override
	public int insertGoods(List<GoodsListEntity> saveGoods) {
		return goodsMapper.insertGoods(saveGoods);
	}

	@Override
	public int delete() {
		return goodsMapper.delete();
	};

	@Override
	public int delete1() {
		return goodsMapper.delete1();
	};

}