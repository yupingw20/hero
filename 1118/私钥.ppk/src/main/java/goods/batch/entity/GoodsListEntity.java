package goods.batch.entity;

import java.text.SimpleDateFormat;

//rid,titles,ctype,keyword,contents,appdate,appip,appuser,modtype,enable,accept,ctype_b,city,
//picname,price,dlei,xlei
public class GoodsListEntity {

	private String  rid;
	private String  titles;
	private String  ctype;	
	private String  keyword; 
	private String  contents;  
	private SimpleDateFormat  appdate;	
	private String  appip;	
	private String  appuser;
	private String  modtype;
	private String  enable;
	private String  accept;
	private int  ctype_b;
	private String  city;
	private String  picname;
	private int  price;
	private int  dlei;
	private int  xlei;

	
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	public String getTitles() {
		return titles;
	}
	public void setTitles(String titles) {
		this.titles = titles;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public SimpleDateFormat getAppdate() {
		return appdate;
	}
	public void setAppdate(SimpleDateFormat timenow) {
		this.appdate = timenow;
	}
	public String getAppip() {
		return appip;
	}
	public void setAppip(String appip) {
		this.appip = appip;
	}
	public String getAppuser() {
		return appuser;
	}
	public void setAppuser(String appuser) {
		this.appuser = appuser;
	}
	public String getModtype() {
		return modtype;
	}
	public void setModtype(String modtype) {
		this.modtype = modtype;
	}
	public String getEnable() {
		return enable;
	}
	public void setEnable(String enable) {
		this.enable = enable;
	}
	public String getAccept() {
		return accept;
	}
	public void setAccept(String accept) {
		this.accept = accept;
	}
	public int getCtype_b() {
		return ctype_b;
	}
	public void setCtype_b(int i) {
		this.ctype_b = i;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPicname() {
		return picname;
	}
	public void setPicname(String picname) {
		this.picname = picname;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getDlei() {
		return dlei;
	}
	public void setDlei(int dlei) {
		this.dlei = dlei;
	}
	public int getXlei() {
		return xlei;
	}
	public void setXlei(int xlei) {
		this.xlei = xlei;
	}
	

}
