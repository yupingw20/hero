package study0601;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

/**
 * An advanced Java program that exports data from any table to CSV file.
 */
public class AdvancedDb2CsvExporter {
    private BufferedWriter fileWriter;
//将表名传递给 export() 方法，它将完成所有工作
    public void export(String table) {
        String jdbcURL = "jdbc:postgresql://localhost:5432/postgres";
        String username = "postgres";
        String password = "root";

//CSV 文件名基于表名生成，后跟 _Export 加当前日期时间作为后缀：
        String csvFileName = getFileName(table.concat("_Export"));

        try (Connection connection = DriverManager.getConnection(jdbcURL, username, password)) {
            String sql = "SELECT * FROM ".concat(table);

            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);

            fileWriter = new BufferedWriter(new FileWriter(csvFileName));

            int columnCount = writeHeaderLine(result);
//next()指针指向下一条记录，有记录（有值）返回true并把记录内容存入到对应的对象中，如果没有返回false。
            while (result.next()) {
                String line = "";

                for (int i = 1; i <= columnCount; i++) {
                    Object valueObject = result.getObject(i);
                    String valueString = "";

                    if (valueObject != null) valueString = valueObject.toString();

                    if (valueObject instanceof String) {
                    	//\"（转义字符,表示字符",因为在双引号里出现双引号，会出现组合混乱，所以转义）
                        valueString = "\"" + escapeDoubleQuotes(valueString) + "\"";
                    }

                    line = line.concat(valueString);

                    if (i != columnCount) {
                        line = line.concat(",");
                    }
                }
//换行
                fileWriter.newLine();

                fileWriter.write(line);
            }

            statement.close();
            fileWriter.close();

        } catch (SQLException e) {
            System.out.println("Datababse error:");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File IO error:");
            e.printStackTrace();
        }

    }

    private String getFileName(String baseName) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String dateTimeInfo = dateFormat.format(new Date());
        //Java字符串格式化
        return baseName.concat(String.format("_%s.csv", dateTimeInfo));
    }
//writeHeaderLine() 方法将列名称写出到 CSV 文件的第一行。
// 它使用 ResultSetMetaData 获取列名，这样它就可以匹配任意表。
    private int writeHeaderLine(ResultSet result) throws SQLException, IOException {
        // write header line containing column names
        ResultSetMetaData metaData = result.getMetaData();
        int numberOfColumns = metaData.getColumnCount();
        String headerLine = "";

        // exclude the first column which is the ID field
        for (int i = 1; i <= numberOfColumns; i++) {
            String columnName = metaData.getColumnName(i);
            headerLine = headerLine.concat(columnName).concat(",");
        }
//substring返回从起始位置（beginIndex）到目标位置（endIndex）之间的字符串，
//但不包含目标位置（endIndex）的字符
        fileWriter.write(headerLine.substring(0, headerLine.length() - 1));

        return numberOfColumns;
    }
//replaceAll是JAVA中常用的替换字符的方法,
//replaceAll(String regex,String replacement)使用给定的 replacement 字符串替换此字符串匹配给定的正则表达式的每个子字符串。
    private String escapeDoubleQuotes(String value) {
    	//\"转义为",\"\"转义为""
        return value.replaceAll("\"", "\"\"");
    }

    public static void main(String[] args) {
        AdvancedDb2CsvExporter exporter = new AdvancedDb2CsvExporter();
        exporter.export("outfile");
        exporter.export("course");

    }
}