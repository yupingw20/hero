package com.example.demo.mapper;

import com.example.demo.bean.HelloBean;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/*Mapper必須是接口interface方法
@Mapper注解：
作用：在接口類上添加了@Mapper，在編譯之後會生成相應的接口實現類
添加位置：接口類上面
*/
@Mapper
public interface HelloMapper {  //HelloMapper的名字(接口名)要和XML文件名一致，大小寫區分

    List<HelloBean> selectEmpAll();

    int insert(HelloBean user);
}