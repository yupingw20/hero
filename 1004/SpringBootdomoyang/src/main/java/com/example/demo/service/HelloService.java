package com.example.demo.service;

import com.example.demo.bean.HelloBean;
import com.example.demo.mapper.HelloMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Service　テーブルより情報を取得する。
 */

//@Service: 注解在類上，表示這是一個業務層bean
@Service
public class HelloService {


    @Autowired
    private HelloMapper helloMapper;

    /**
     * テーブルより情報を取得する。
     * @return 社員情報
     */
    public List<HelloBean> selectName()
    {
        return helloMapper.selectEmpAll();
    }
    public int insert(HelloBean user) {
        return helloMapper.insert(user);
    }

}