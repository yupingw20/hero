package com.example.demo.bean;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/*在Spring中，只要一個類能被實例化，並被Spring容器管理，這個類就稱為一個Bean,或者SpringBean.*/

//要和數據庫的字段(列名)一致,字段不一致時要用mybatis修改
@Getter
@Setter

public class HelloBean {
	// 社員番号
    @NotNull(message = "id不能为空")
	private int id;
	// 社員名
	private String vName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getvName() {
		return vName;
	}
	public void setvName(String vName) {
		this.vName = vName;
	}

}
