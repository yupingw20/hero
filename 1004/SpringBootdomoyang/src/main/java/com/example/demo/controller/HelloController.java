package com.example.demo.controller;

import com.example.demo.bean.HelloBean;
import com.example.demo.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/*controller裡面調用的是service
@Controller：用於定義控制器類，在spring 項目中由控制器負責將用戶發來的URL請求轉發到
對應的服務接口（service層），一般這個注解在類中，通常方法需要配合注解@RequestMapping。
*/
@Controller
public class HelloController {

//@Autowired：自動導入依賴的bean
    @Autowired
    private HelloService helloService;

//@RequestMapping：提供路由信息，負責URL到Controller中的具體函數的映射。
    @RequestMapping(value="list")
    public String list (Model model) {
        List<HelloBean> list = helloService.selectName();
        model.addAttribute("list",list);  //把取出紀錄返回到畫面，雙引號裡面的字必須跟html裡面的相對應
        return "list";
    }

    @RequestMapping(value="add")
    public String add() {
        return "add";  //轉跳新規登錄頁面
    }

    @RequestMapping(value="insert")
    public String insert(@ModelAttribute HelloBean user){
        helloService.insert(user);  //把在頁面上輸入的數據登入到數據庫
        return "redirect:/list" ; //跳轉到一覽頁面
    }
}