package com.example.demo;
//Spring Boot 項目通常有一個名為 *Application 的入口類，入口類里有一個 main 方法，
// 這個 main 方法其實就是一個標準的 Javay 應用的入口方法。
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

/*
包含了@ComponentScan、@Configuration和@EnableAutoConfiguration注解,
由於這些註解一般都是一起使用，spring boot提供了一個統一的註解。
@ComponentScan(組件掃描)：组件扫描，可自动发现和装配一些Bean。
@Configuration(配置)：等同于spring的XML配置文件；使用Java代码可以检查类型安全。
@EnableAutoConfiguration：啟用自動配置。
*/
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	//@Bean:相当于XML中的,放在方法的上面，而不是类，意思是产生一个bean,并交给spring管理。
	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
		final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		// コンフィグファイルの読み込み
		sessionFactory.setConfigLocation(new ClassPathResource("/mybatis-config.xml"));

		return sessionFactory.getObject();
	}

}
