create table student( 
  Sid int
  , Sname varchar (20)
  , Sage date
  , Ssex varchar (20)
  , primary key (Sid)
); 

insert 
into student(Sid, Sname, Sage, Ssex) 
values (1, '?', '1990-01-01', 'j') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (2, '??', '1990-12-21', 'j') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (3, '??', '1990-12-20', 'j') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (4, ']', '1990-01-01', 'j') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (5, 'ü~', '1991-12-01', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (6, '??', '1992-01-01', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (7, '?|', '1989-01-01', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (9, '?O', '2017-12-20', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (10, 'l', '2017-12-25', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (11, 'l', '2012-06-06', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (12, '?Z', '2013-06-13', '') 
insert 
into student(Sid, Sname, Sage, Ssex) 
values (13, '?µ', '2014-06-01', '') create table Course( 
  Cid int
  , Cname varchar (20)
  , Tid int
  , primary key (Cid)
); 

insert 
into Course(Cid, Cname, Tid) 
values (1, '?¶', 1) 
insert 
into Course(Cid, Cname, Tid) 
values (2, 'w', 2) 
insert 
into Course(Cid, Cname, Tid) 
values (3, 'p?', 3) create table Teacher(Tid int, Tname varchar (20), primary key (Tid)); 

insert 
into Teacher(Tid, Tname) 
values (1, '?O') 
insert 
into Teacher(Tid, Tname) 
values (2, 'l') 
insert 
into Teacher(Tid, Tname) 
values (3, '¤Ü') drop table sc create table SC( 
  Sid varchar (20)
  , Cid varchar (20)
  , Score varchar (20)
) 
insert 
into SC(Sid, Cid, Score) 
values (1, 1, 80); 

insert 
into SC(Sid, Cid, Score) 
values (1, 2, 90); 

insert 
into SC(Sid, Cid, Score) 
values (1, 3, 99); 

insert 
into SC(Sid, Cid, Score) 
values (2, 1, 70); 

insert 
into SC(Sid, Cid, Score) 
values (2, 2, 60); 

insert 
into SC(Sid, Cid, Score) 
values (2, 3, 80); 

insert 
into SC(Sid, Cid, Score) 
values (3, 1, 80); 

insert 
into SC(Sid, Cid, Score) 
values (3, 2, 80); 

insert 
into SC(Sid, Cid, Score) 
values (3, 3, 80); 

insert 
into SC(Sid, Cid, Score) 
values (4, 1, 50); 

insert 
into SC(Sid, Cid, Score) 
values (4, 2, 30); 

insert 
into SC(Sid, Cid, Score) 
values (4, 3, 20); 

insert 
into SC(Sid, Cid, Score) 
values (5, 1, 76); 

insert 
into SC(Sid, Cid, Score) 
values (5, 2, 87); 

insert 
into SC(Sid, Cid, Score) 
values (6, 1, 31); 

insert 
into SC(Sid, Cid, Score) 
values (6, 3, 34); 

insert 
into SC(Sid, Cid, Score) 
values (7, 2, 89); 

insert 
into SC(Sid, Cid, Score) 
values (7, 3, 98); 

select
  * 
from
  student 
where
  sname like '%' 
  and ssex = 'j' 
select
  * 
from
  sc 
where
  score between '80' and '90' 
select
  student.sid
  , student.sname
  , sc.score 
from
  student
  , sc 
where
  student.sid = 2 
  and student.Sid = sc.Sid 
select
  s.Sid
  , s.Sname as name
  , sc.Score 
from
  student as s
  , sc 
where
  s.sid = 2 
  and s.Sid = sc.Sid 
select
  s.Sid
  , s.Sname as name
  , sc.Score 
from
  student as s 
  inner join sc 
    on s.Sid = sc.Sid 
where
  s.Sid = 13 
select
  s.Sid
  , s.Sname as name
  , sc.Score 
from
  student as s 
  left join sc 
    on s.Sid = sc.Sid 
select
  s.sname
  , c.cname
  , t.tname
  , sc.Score 
from
  student as s 
  left join sc 
    on s.Sid = sc.Sid 
  inner join course as c 
    on sc.Cid = c.Cid 
  inner join teacher as t 
    on c.Tid = t.Tid 
select
  sc.sid
  , s.sname
  , c.cname
  , t.tname
  , sc.Score 
from
  student as s 
  left join sc 
    on s.Sid = sc.Sid 
  inner join course as c 
    on sc.Cid = c.Cid 
  inner join teacher as t 
    on c.Tid = t.Tid 
select
  sc.sid
  , c.cname
  , t.tname
  , sc.Score 
from
  sc 
  inner join course as c 
    on sc.Cid = c.Cid 
  inner join teacher as t 
    on c.Tid = t.Tid 
select
  s.Sname
  , x.cname
  , x.tname
  , x.Score 
from
  student as s 
  left join ( 
    select
      sc.sid
      , c.cname
      , t.tname
      , sc.Score 
    from
      sc 
      inner join course as c 
        on sc.Cid = c.Cid 
      inner join teacher as t 
        on c.Tid = t.Tid
  ) as x 
    on s.Sid = x.sid 
select
  sid.AVG(score) as a 
from
  sc 
group by
  sid 
having
  AVG(score) > 80 
select
  count(score) 
from
  sc 
where
  sid = '2' 
select
  count(*) 
from
  student 
select
  count(distinct sname) 
from
  student 
select
  min(score) 
from
  sc 
where
  cid = '1' 
select
  sum(score) 
from
  sc 
where
  cid = '1' 
select
  max(score) 
from
  sc 
group by
  cid 
select
  sum(score) 
from
  sc 
group by
  cid                                             --æ1?
  select
  sc.Score
  , sc.Sid
  , sc.Cid 
from
  sc 
where
  cid = '1' 
select
  sc.Score
  , sc.Sid
  , sc.Cid 
from
  sc 
where
  cid = '2' 
select
  s.Sname
  , c1.Cid
  , c1.Score
  , c2.Cid
  , c2.Score 
from
  student as s 
  inner join ( 
    select
      sc.Score
      , sc.Sid
      , sc.Cid 
    from
      sc 
    where
      cid = '1'
  ) as c1 
    on s.Sid = c1.Sid 
  inner join ( 
    select
      sc.Score
      , sc.Sid
      , sc.Cid 
    from
      sc 
    where
      cid = '2'
  ) as c2 
    on c1.Sid = c2.Sid 
where
  c1.Score > c2.Score 
select
  s.Sname
  , c1.Cid
  , c1.Score
  , c2.Cid
  , c2.Score 
from
  student as s 
  inner join ( 
    select
      sc.Score
      , sc.Sid
      , sc.Cid 
    from
      sc 
    where
      cid = '1'
  ) as c1 
    on s.Sid = c1.Sid 
  left join ( 
    select
      sc.Score
      , sc.Sid
      , sc.Cid 
    from
      sc 
    where
      cid = '2'
  ) as c2 
    on c1.Sid = c2.Sid 
select
  s.Sname
  , s.Sid 
from
  student as s 
  left join sc 
    on s.Sid = sc.Sid 
select
  sid
  , avg(score) 
from
  sc 
group by
  sid 
having
  avg(score) >= 60 
select
  s.Sid
  , s.Sname
  , b.x 
from
  student as s 
  inner join ( 
    select
      sid
      , avg(score) as x 
    from
      sc 
    group by
      sid 
    having
      avg(score) >= 60
  ) as b 
    on s.Sid = b.sid 
select
  sid
  , count(cid)
  , sum(score) 
from
  sc 
group by
  sid                                             --1
  SELECT
  S.sname
  , C1.cid AS S1
  , C1.score AS SC1
  , C2.cid AS S2
  , C2.score AS SC2 
FROM
  STUDENT AS S 
  INNER JOIN ( 
    SELECT
      SC.SID
      , SC.cid
      , SC.score 
    FROM
      SC 
    WHERE
      SC.CID = '01'
  ) AS C1 
    ON S.sid = C1.SID 
  INNER JOIN ( 
    SELECT
      SC.SID
      , SC.cid
      , SC.score 
    FROM
      SC 
    WHERE
      SC.CID = '02'
  ) AS C2 
    ON C1.SID = C2.SID 
WHERE
  C1.score > C2.score                             --7
  select
  s.Sid
  , s.Sname
  , x.y
  , x.z 
from
  student as s 
  left join ( 
    select
      sid
      , count(cid) as y
      , sum(score) as z 
    from
      sc 
    group by
      sid
  ) as x 
    on s.Sid = x.sid                              --7.1
    select
  sc.Sid
  , sc.Score
  , c.Cname 
from
  sc 
  inner join course as c 
    on sc.Cid = c.Cid 
select
  s.Sid
  , s.Sname
  , s.Sage
  , s.Ssex
  , sc.Score 
from
  student as s 
  inner join sc 
    on s.Sid = sc.Sid                             --8??uv©V?IÊ
    select
  count(tname) 
from
  teacher 
where
  tname like '%'                                 --9??w?u?OvV?ö?I¯wIM§
  select
  t.tname
  , c.cid 
from
  teacher as t 
  inner join course as c 
    on t.tid = c.tid 
select
  sc.sid
  , x.tname 
from
  sc 
  inner join ( 
    select
      t.tname
      , c.cid 
    from
      teacher as t 
      inner join course as c 
        on t.tid = c.tid
  ) as x 
    on sc.cid = x.cid 
select
  s.sname
  , s.sage
  , s.ssex
  , y.tname 
from
  student as s 
  inner join ( 
    select
      sc.sid
      , x.tname 
    from
      sc 
      inner join ( 
        select
          t.tname
          , c.cid 
        from
          teacher as t 
          inner join course as c 
            on t.tid = c.tid
      ) as x 
        on sc.cid = x.cid
  ) as y 
    on s.sid = y.sid 
where
  y.tname = '?O'                                  --10??vLwSL?öI¯wIM§
  select
  s.Sid
  , s.Sname
  , s.Sage
  , s.Ssex 
from
  student as s 
  inner join sc 
    on s.Sid = sc.Sid 
  inner join course as c 
    on sc.Cid = c.Cid 
select
  count(sname)
  , sname
  , sid
  , sage
  , ssex 
from
  ( 
    select
      s.Sid
      , s.Sname
      , s.Sage
      , s.Ssex 
    from
      student as s 
      inner join sc 
        on s.Sid = sc.Sid 
      inner join course as c 
        on sc.Cid = c.Cid
  ) as x 
group by
  sname
  , sid 
having
  count(sname) < (select count(cid) from course)  --11??­Lê??^w?" 01 "I¯ww¯I¯wIM§
  --w?01Iw¶
  select
  sid
  , sname 
from
  student as s 
where
  sid = '01'                                      --Qcid
  select
  a.sid
  , a.sname
  , sc.cid 
from
  ( 
    select
      sid
      , sname 
    from
      student as s 
    where
      sid = '01'
  ) as a 
  inner join sc 
    on a.sid = sc.sid                             --scïÜ010203I
    select
  sc.sid
  , sc.cid 
from
  sc 
  inner join ( 
    select
      a.sid
      , a.sname
      , sc.cid 
    from
      ( 
        select
          sid
          , sname 
        from
          student as s 
        where
          sid = '01'
      ) as a 
      inner join sc 
        on a.sid = sc.sid
  ) as a 
    on sc.cid = a.cid 
select distinct
  s.sname
  , s.sage
  , s.ssex 
from
  student as s 
  inner join ( 
    select
      sc.sid
      , sc.cid 
    from
      sc 
      inner join ( 
        select
          a.sid
          , a.sname
          , sc.cid 
        from
          ( 
            select
              sid
              , sname 
            from
              student as s 
            where
              sid = '01'
          ) as a 
          inner join sc 
            on a.sid = sc.sid
      ) as a 
        on sc.cid = a.cid
  ) as b 
    on s.sid = b.sid                              --12??a" 01 "I¯ww?I?ö ®S¯I´¼¯wIM§
    --LlIcid
    select
  s.Sid
  , s.Sname
  , s.Sage
  , s.Ssex
  , c.Cid 
from
  student as s 
  left join sc 
    on s.Sid = sc.Sid 
  left join course as c 
    on sc.Cid = c.Cid 
group by
  s.Sid
  , c.Cid                                         --1Icid
  select
  sc.Sid
  , sc.Cid 
from
  sc 
where
  sid = 1 
select
  a.Cid
  , b.Sid
  , b.Sname
  , b.Sage
  , b.Ssex 
from
  (select sc.Sid, sc.Cid from sc where sid = 1) as a 
  full join ( 
    select
      s.Sid
      , s.Sname
      , s.Sage
      , s.Ssex
      , c.Cid 
    from
      student as s 
      left join sc 
        on s.Sid = sc.Sid 
      left join course as c 
        on sc.Cid = c.Cid 
    group by
      s.Sid
      , c.Cid
  ) as b 
    on a.Cid = b.Cid                              --13??vw?"?O"V??öICê??öIw¶©¼
    select distinct
  sc.sid 
from
  sc 
  inner join course as c 
    on sc.cid = c.cid 
  inner join teacher as t 
    on c.tid = t.tid 
where
  Tname = '?O' 
select
  sname 
from
  student 
where
  sid not in ( 
    select distinct
      sc.sid 
    from
      sc 
      inner join course as c 
        on sc.cid = c.cid 
      inner join teacher as t 
        on c.tid = t.tid 
    where
      Tname = '?O'
  )                                               --14.????y´Èãsyi?öI¯wIwC©¼y´½Ï¬?
  --¬°60ªIsid, cid ,score
  select
  sc.Sid
  , sc.Cid
  , sc.Score 
from
  sc 
where
  sc.Score < 60 
select
  s.Sid
  , s.Sname
  , avg(a.Score) 
from
  student as s 
  inner join ( 
    select
      sc.Sid
      , sc.Cid
      , sc.Score 
    from
      sc 
    where
      sc.Score < 60
  ) as a 
    on s.Sid = a.Sid 
group by
  sid 
having
  count(a.sid) >= 2                               --15.?õh 01 "?öª¬° 60CÂª~rñIw¶M§
  --¬°6001M§
  select
  sc.Sid
  , sc.Score 
from
  sc 
where
  sc.Score <= 60 
  and cid = 1 
select
  s.Sid
  , s.Sname
  , s.Sage
  , s.Ssex
  , a.Score 
from
  student as s 
  inner join ( 
    select
      sc.Sid
      , sc.Score 
    from
      sc 
    where
      sc.Score <= 60 
      and cid = 1
  ) as a 
    on s.Sid = a.Sid 
order by
  a.Score desc                                    --16.Â½Ï¬?¸á?¦Lw¶IL?öI¬?Èy½Ï¬?
  select
  s.Sid
  , s.Sname
  , avg(score) 
from
  student as s 
  left join sc 
    on s.Sid = sc.Sid 
group by
  sid 
select distinct
  a.Sid
  , a.Sname
  , ifnull(a.x, 0) as y
  , ifnull(Score, 0) as score 
from
  ( 
    select
      s.Sid
      , s.Sname
      , avg(score) as x 
    from
      student as s 
      left join sc 
        on s.Sid = sc.Sid 
    group by
      sid
  ) as a 
  left join sc 
    on a.Sid = sc.Sid 
order by
  y desc                     
                       --10.25
  CREATE TABLE account( 
    id INT (11) NOT NULL AUTO_INCREMENT
    , name VARCHAR (50) NOT NULL
    , created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
      ON UPDATE CURRENT_TIMESTAMP
      , updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
        ON UPDATE CURRENT_TIMESTAMP
        , PRIMARY KEY (id)
  ) CREATE TABLE sample( 
    id INT (11) NOT NULL AUTO_INCREMENT
    , value INT (5) NOT NULL DEFAULT 0
    , PRIMARY KEY (id)
  ) 
INSERT 
INTO sample(value) 
VALUES (1)
  , (2)
  , (3)
  , (4)
  , (5)
  , (6)
  , (7)
  , (8)
  , (9)
  , (10) 
  
  
  
  create table uuser( 
  id int (20) not null auto_increment primary key
  , name varchar (20)
  , age int
)  


create table product( 
  id int auto_increment primary key 
  , name varchar (20)
  , price int
) 






SELECT
  @rownum := @rownum + 1 
FROM
  sample
  , (SELECT @rownum := 10000) AS v; 

SELECT
  @rownum := @rownum + 1 
FROM
  sample AS s1
  , sample AS s2
  , (SELECT @rownum := 10000) AS v; 

SELECT
  ROW_NUMBER() OVER ()
  , ROW_NUMBER() OVER () + 10000 
FROM
  sample AS s1
  , sample AS s2 
  
  
  
INSERT 
INTO uuser(name) 
SELECT
  CONCAT('name', @rownum := @rownum + 1) 
FROM
  sample AS s1
  , sample AS s2
  , sample AS s3
  , sample AS s4
  , sample AS s5
  , sample AS s6
  , (SELECT @rownum := 0) AS v
  
  
  
  INSERT 
INTO product(name) 
SELECT
  CONCAT('name', @rownum := @rownum + 1) 
FROM
  sample AS s1
  , sample AS s2
  , sample AS s3
  , sample AS s4
  , sample AS s5
  , sample AS s6
  , (SELECT @rownum := 0) AS v
  
  
  
  
  
  create table purchase( 
  id int  auto_increment primary key  
  , pur_date timestamp default current_timestamp 
  , user_id int 
  , prouduct_id int 
) 
drop table purchase
  
INSERT INTO purchase(user_id,prouduct_id) 
SELECT
  ( @rownum := @rownum + 1),( @rownum := @rownum )
FROM
  sample AS s1
  , sample AS s2
  , sample AS s3
  , sample AS s4
  , sample AS s5
    , sample AS s6,
  (SELECT @rownum := 0) AS v
  
  
  select u.id,u.name,u.age
  from uuser as u
  inner join purchase as p
  on u.id=p.id


SELECT *
FROM uuser
WHERE id IN (SELECT id FROM purchase)

