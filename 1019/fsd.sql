SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `add_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '¾?l',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'è÷',
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '¾?n¬',
  `add_details` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '?î?',
  `u_id` int(11) NULL DEFAULT NULL COMMENT 'q?id',
  `is_default` tinyint(4) NULL DEFAULT 0 COMMENT '¥ÛàÒ?¾?n¬',
  PRIMARY KEY (`add_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of address
-- ----------------------------

-- ----------------------------
-- Table structure for fruits
-- ----------------------------
DROP TABLE IF EXISTS `fruits`;
CREATE TABLE `fruits`  (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '??',
  `specification` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '?i',
  `supplier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '?¤',
  `place_origin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '?n',
  `price` int(11) NULL DEFAULT NULL COMMENT 'Ái(Ñ)',
  `price_off` int(11) NULL DEFAULT NULL COMMENT 'ÜJÁ',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '¤iW¦¬?Ð',
  PRIMARY KEY (`f_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fruits
-- ----------------------------
INSERT INTO `fruits` VALUES (1, 'V?Â?', '500g/á´', '??I', '?¹', 150, 145, '/del/fruits-img7.png');
INSERT INTO `fruits` VALUES (2, 'V?òq', '500g/á´', '??I', '?¹', 150, 145, '/del/fruits-img4.png');
INSERT INTO `fruits` VALUES (3, 'V??àË', '500g/á´', '??I', '?¹', 150, 145, '/del/fruits-img2.png');

-- ----------------------------
-- Table structure for fruits_image_details
-- ----------------------------
DROP TABLE IF EXISTS `fruits_image_details`;
CREATE TABLE `fruits_image_details`  (
  `fid_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Ê?î?n¬',
  `f_id` int(11) NULL DEFAULT NULL COMMENT 'Êid',
  PRIMARY KEY (`fid_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fruits_image_details
-- ----------------------------
INSERT INTO `fruits_image_details` VALUES (1, '/del/fruits-img7.png', 1);
INSERT INTO `fruits_image_details` VALUES (2, '/del/fruits-img4.png', 2);
INSERT INTO `fruits_image_details` VALUES (3, '/del/fruits-img2.png', 3);

-- ----------------------------
-- Table structure for fruits_images
-- ----------------------------
DROP TABLE IF EXISTS `fruits_images`;
CREATE TABLE `fruits_images`  (
  `fi_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Ê?Ðn¬',
  `f_id` int(11) NULL DEFAULT NULL COMMENT 'Êid',
  PRIMARY KEY (`fi_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fruits_images
-- ----------------------------
INSERT INTO `fruits_images` VALUES (1, '/del/special-sale-img1.jpg', 1);
INSERT INTO `fruits_images` VALUES (2, '/del/special-sale-img4.jpg', 2);
INSERT INTO `fruits_images` VALUES (3, '/del/special-sale-img5.jpg', 3);

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`  (
  `oi_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `o_id` int(11) NULL DEFAULT NULL COMMENT '???',
  `f_id` int(11) NULL DEFAULT NULL COMMENT '¤i?',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '¤i??',
  `price` int(11) NULL DEFAULT NULL COMMENT '¤iÁi',
  `num` int(11) NULL DEFAULT NULL COMMENT '??Ê',
  `is_review` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '¥Û?Á',
  `is_deliver` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '¥Û??',
  `is_take` tinyint(4) NULL DEFAULT 0 COMMENT '¥Û¾?',
  PRIMARY KEY (`oi_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES (128, 119, 2, 'V?òq 500g/á´', 145, 2, 1, 1, 1);
INSERT INTO `order_item` VALUES (129, 120, 2, 'V?òq 500g/á´', 145, 2, 1, 1, 1);
INSERT INTO `order_item` VALUES (130, 120, 3, 'V??àË 500g/á´', 145, 2, 1, 1, 1);
INSERT INTO `order_item` VALUES (131, 121, 2, 'V?òq 500g/á´', 145, 2, 0, 0, 0);
INSERT INTO `order_item` VALUES (132, 122, 2, 'V?òq 500g/á´', 145, 2, 0, 0, 0);
INSERT INTO `order_item` VALUES (134, 124, 1, 'V?Â? 500g/á´', 145, 1, 0, 1, 1);
INSERT INTO `order_item` VALUES (135, 125, 1, 'V?Â? 500g/á´', 145, 1, 0, 1, 0);
INSERT INTO `order_item` VALUES (141, 130, 1, 'V?Â? 500g/á´', 145, 1, 0, 0, 0);
INSERT INTO `order_item` VALUES (142, 131, 3, 'V??àË 500g/á´', 145, 3, 0, 0, 0);
INSERT INTO `order_item` VALUES (143, 132, 1, 'V?Â? 500g/á´', 145, 1, 0, 0, 0);
INSERT INTO `order_item` VALUES (145, 134, 1, 'V?Â? 500g/á´', 145, 2, 0, 0, 0);
INSERT INTO `order_item` VALUES (146, 135, 2, 'V?òq 500g/á´', 145, 1, 0, 0, 0);
INSERT INTO `order_item` VALUES (147, 136, 2, 'V?òq 500g/á´', 145, 1, 0, 0, 0);
INSERT INTO `order_item` VALUES (148, 137, 2, 'V?òq 500g/á´', 145, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review`  (
  `r_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `u_id` int(11) NULL DEFAULT NULL COMMENT 'q?id',
  `f_id` int(11) NULL DEFAULT NULL COMMENT '¤iid',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '??àe',
  `is_anonymous` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '¥Û½¼??',
  `r_time` datetime NULL DEFAULT NULL COMMENT '????',
  PRIMARY KEY (`r_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of review
-- ----------------------------

-- ----------------------------
-- Table structure for shop_cart
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart`  (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `u_id` int(11) NULL DEFAULT NULL COMMENT 'q?id',
  `f_id` int(11) NULL DEFAULT NULL COMMENT '¤iid',
  `num` int(11) NULL DEFAULT NULL COMMENT '¤iÊ',
  PRIMARY KEY (`sc_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shop_cart
-- ----------------------------

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `o_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `u_id` int(11) NULL DEFAULT NULL COMMENT 'q?id',
  `add_id` int(11) NULL DEFAULT NULL COMMENT '¾?n¬id ',
  `is_play` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '¥Ût¼',
  `o_time` datetime NULL DEFAULT NULL COMMENT '????',
  `total_money` int(11) NULL DEFAULT NULL COMMENT '??à?(?Ê/Ñ)',
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '???',
  PRIMARY KEY (`o_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 138 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_order
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `u_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '?',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'p?¼',
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '§?',
  `head_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '?',
  `account` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '?',
  PRIMARY KEY (`u_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
