import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


//StrictMode 是一个用来检查项目中潜在问题的工具
//StrictMode 不会渲染任何真实的UI。它为其后代元素触发额外的检查和警告。
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
