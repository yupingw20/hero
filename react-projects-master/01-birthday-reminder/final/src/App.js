import React, { useState } from 'react'
import data from './data'
import List from './List'


//使用 state hook 函数加载 data.js  文件中的数据
//State Hook在不编写 class 的情况下使用 state 以及其他的 React 特性
//useState() 方法里面唯一的参数就是初始 state。
//声明了一个叫 people 的 state 变量，然后把它设为 data,通过调用 setPeople 来更新当前的 people
function App() {
  const [people, setPeople] = useState(data)
  //<section> 标签定义文档中的节（section、区段）。比如章节、页眉、页脚或文档中的其他部分。
  return (
    //使用 setPeople([]) 方法，将列表的数据清空，界面将会重新 re-render
    <main>
      <section className='container'>
        <h3>{people.length} birthdays today</h3>
        <List people={people} />
        <button onClick={() => setPeople([])}>clear all</button>
      </section>
    </main>
  )
}

export default App
