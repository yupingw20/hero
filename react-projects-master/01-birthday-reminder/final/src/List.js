import React from 'react';

//展示用户的列表信息，组件定义了 people 属性，用于接收 data 的数据，进行渲染列表数据。
//map,使用map遍历时,需要给子元素添加一个key,作为唯一的标识
//使用 const {id,name,age,image} =person 来结构化 person的属性
// alt 属性可以为图像提供替代的信息
const List = ({ people }) => {
  return (
    <>
      {people.map((person) => {
        const { id, name, age, image } = person;
        return (
          <article key={id} className='person'>
            <img src={image} alt={name} />
            <div>
              <h4>{name}</h4>
              <p>{age} years</p>
            </div>
          </article>
        );
      })}
    </>
  );
};

export default List;
