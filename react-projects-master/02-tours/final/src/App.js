import React, { useState, useEffect } from 'react'
import Loading from './Loading'
import Tours from './Tours'
// ATTENTION!!!!!!!!!!
// I SWITCHED TO PERMANENT DOMAIN

//定义接口地址 URL 变量
const url = 'https://course-api.com/react-tours-project'
//定义加载数据状态和清单数据状态（state hook）：loading 和 tours，用来显示加载状态和渲染接口的数据
function App() {
  const [loading, setLoading] = useState(true)
  const [tours, setTours] = useState([])
//定义 removeTour 事件，使用 filter 属性删除对应清单
  const removeTour = (id) => {
    const newTours = tours.filter((tour) => tour.id !== id)
    setTours(newTours)
  }
  // 接下来定义接口请求方法 fetchTours，使用 async，await 方式请求接口，
  // 通过 useEffect Hook 调用 fetchTours 方法，
  // 最后别忘记 useEffect 的第二个参数 [] 为空数组，只加载一次
  const fetchTours = async () => {
    setLoading(true)
    try {
      const response = await fetch(url)
      const tours = await response.json()
      setLoading(false)
      setTours(tours)
    } catch (error) {
      setLoading(false)
      console.log(error)
    }
  }
  useEffect(() => {
    fetchTours()
  }, [])
  if (loading) {
    return (
      <main>
        <Loading />
      </main>
    )
  }
  if (tours.length === 0) {
    return (
      <main>
        <div className='title'>
          <h2>no tours left</h2>
          <button className='btn' onClick={() => fetchTours()}>
            refresh
          </button>
        </div>
      </main>
    )
  }
  return (
    <main>
      <Tours tours={tours} removeTour={removeTour} />
    </main>
  )
}

export default App
