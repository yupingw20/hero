import React, { useState } from 'react';
import data from './data';
import SingleQuestion from './Question';


// 引入本地数据文件 data.js 和 Question 组件，定义 questions 状态变量（state hook），
// 初始数据为  data.js 的数据，然后通过数组的  map 方法迭代，将数据渲染至 Question 组件
function App() {
  const [questions, setQuestions] = useState(data);
  return (
    <main>
      <div className='container'>
        <h3>questions and answers about login</h3>
        <section className='info'>
          {questions.map((question) => {
            return (
              <SingleQuestion key={question.id} {...question}></SingleQuestion>
            );
          })}
        </section>
      </div>
    </main>
  );
}

export default App;
