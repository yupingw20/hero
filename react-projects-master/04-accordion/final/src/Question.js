import React, { useState } from 'react';
import { AiOutlineMinus, AiOutlinePlus } from 'react-icons/ai';




// 定义单项问题组件 Question，新建 Question.js 文件，用于显示单个问题项，
// 这里定义组件的 title 标题属性，info 答案详情属性，
// 我们可以通过父组件传值的形式将内容渲染，同时我们定义了 showInfo 数据状态变量，
// 通过更改数据状态的真假状态实现问题答案的折叠。 
// 如果条件是 true，&& 右侧的元素就会被渲染，如果是 false，React 会忽略并跳过它。
const Question = ({ title, info }) => {
  const [showInfo, setShowInfo] = useState(false);
  return (
    <article className='question'>
      <header>
        <h4>{title}</h4>
        <button className='btn' onClick={() => setShowInfo(!showInfo)}>
          {showInfo ? <AiOutlineMinus /> : <AiOutlinePlus />}
        </button>
      </header>
      {showInfo && <p>{info}</p>}
    </article>
  );
};

export default Question;
