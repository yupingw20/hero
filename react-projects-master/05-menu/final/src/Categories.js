import React from 'react';



//这个组件定义了分类属性categories，用来接收父组件传递的数据，
// 同时定义 filterItems 事件属性，将当前选择的分类传递给父组件
const Categories = ({ categories, filterItems }) => {
  return (
    <div className="btn-container">
      {categories.map((category, index) => {
        return (
          <button
            type="button"
            className="filter-btn"
            key={index}
            onClick={() => filterItems(category)}
          >
            {category}
          </button>
        );
      })}
    </div>
  );
};

export default Categories;
