import React, { useState } from 'react';
import Menu from './Menu';
import Categories from './Categories';
import items from './data';


// 定义 allCategories 分类数组变量，对本地数据的分类进行去重，显示所有美食的分类
// 定义 menuItems 美食数据状态变量和 categories 分类数据变量，
// 并分别初始化为所有的美食数据和所有的分类数据。
// 定义 filterItems 事件函数，接收子组件 Categories 传递过来的分类属性，
// 动态的更改当前分类下的美食数据，重新 re-render 页面数据
//把Array的某些元素过滤掉，然后返回剩下的元素。
const allCategories = ['all', ...new Set(items.map((item) => item.category))];

function App() {
  const [menuItems, setMenuItems] = useState(items);
  const [categories, setCategories] = useState(allCategories);

  const filterItems = (category) => {
    if (category === 'all') {
      setMenuItems(items);
      return;
    }
    const newItems = items.filter((item) => item.category === category);
    setMenuItems(newItems);
  };

  return (
    <main>
      <section className="menu section">
        <div className="title">
          <h2>our menu</h2>
          <div className="underline"></div>
        </div>
        <Categories categories={categories} filterItems={filterItems} />
        <Menu items={menuItems} />
      </section>
    </main>
  );
}

export default App;
