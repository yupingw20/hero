package study0601;

/*==============================*
 * 
 * 
 * 
 *=============================*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * CSVファイルを�iみ�zみ、DBに鞠�hする
 * 
 * @author c
 * @version 1.0.0
 */
public class Study060105 {

	public static void main(String[] args) throws SecurityException, IOException {
		/**
		 * ログの兜豚�I尖 ログレベル�O協
		 */
		Logger logger = Logger.getLogger("log-Test");
		logger.setLevel(Level.ALL);
		Date logDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmm");
		FileHandler fileHandler = new FileHandler("C:\\Users\\compu\\Desktop/Log"+dateFormat.format(logDate)+".log");
		logger.addHandler(fileHandler);
		fileHandler.setLevel(Level.ALL);
		Formatter formatter = new SimpleFormatter();
		fileHandler.setFormatter(formatter);
		try {
			logger.info("�I尖を�_兵しました。");
			// JDBCドライバをロ�`ド
			Class.forName("org.postgresql.Driver");
			// DBに俊�A
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"root");
			// コネクションの函誼
			Statement stmt = conn.createStatement();

			// CSVファイルを�iみ�zみする
			try {
				File f = new File("C:\\Users\\compu\\Desktop\\infile.csv");
				BufferedReader br = new BufferedReader(new FileReader(f));
				int countNum = 0;
				String line;
				// SQLの�g佩
				while ((line = br.readLine()) != null) {
					String[] data = line.split(",", 0);
					countNum++;
					stmt.executeUpdate("INSERT INTO outfile(pid,picname,rid,pictxt,ord) VALUES (" + data[0] + ","
							+ data[1] + "," + data[2] + "," + data[3] + "," + data[4] + ");");
				}
				logger.info(countNum+"周デ�`タが�iみ函りました。");
				br.close();
				logger.info("屎械に�K阻しました。");
			} catch (IOException e) {
				logger.log(Level.SEVERE, "ファイルの�iみ函りに払�，靴泙靴�", e);
			}
			// ステ�`トメントとコレクションのclose
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.WARNING, "エラ�`が�k伏しました。", e);
			System.out.println(e);
		}
	}

}
