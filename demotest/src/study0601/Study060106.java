package study0601;
 
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class Study060106 {
 
     public static void main(String[] args) throws RowsExceededException, WriteException, IOException {
             
            //1. 导出Excel的路径
            String filePath = "C:\\Users\\compu\\Desktop\\infile2.xls";
            WritableWorkbook wwb =null;
             
            try {
                wwb = Workbook.createWorkbook(new File(filePath));
            } catch (Exception e) {
                e.printStackTrace();
            }
             
            //创建Excel表的"学生"区域的数据
            WritableSheet sheet = wwb.createSheet("test",0);//或者rwb.getSheet(0)获取第一个区域
            try {
                //2. 连接数据库的几行代码
                Connection con = null; 
                PreparedStatement ps = null;
                ResultSet rs = null;
                String url = "jdbc:postgresql://localhost:5432/postgres";
                String sql = "select * from outfile where pid > 10";
                con = DriverManager.getConnection(url, "postgres", "root");
                ps = con.prepareStatement(sql);// SQL预处理 
                rs = ps.executeQuery();
                //ResultSet是数据库中的数据，将其转换为List类型
                List<outfiles> list = new ArrayList<outfiles>();
                while(rs.next()){
                	outfiles stu = new outfiles();
                    stu.setPid(rs.getInt("pid"));
                    stu.setPicName(rs.getString("picname"));
                    stu.setRid(rs.getInt("rid"));
                    stu.setPicTxt(rs.getString("pictxt"));
                    stu.setOrd(rs.getInt("ord"));
                    list.add(stu);
                }
                ps.close(); 
                con.close();         
                for(int i = 0; i<list.size(); i++){
                    //Number对应数据库的int类型数据
                    sheet.addCell(new jxl.write.Number(0,i,list.get(i).getPid()));
                    sheet.addCell(new jxl.write.Number(2,i,list.get(i).getRid()));
                    sheet.addCell(new jxl.write.Number(4,i,list.get(i).getOrd()));
                    //Label对应数据库String类型数据
                    sheet.addCell(new Label(1,i,list.get(i).getPicName()));
                    sheet.addCell(new Label(3,i,list.get(i).getPicTxt()));
                }
                wwb.write();
                 
            } catch (SQLException e) {
                e.printStackTrace();
            }finally{
                wwb.close();
            }
             
             
        }
 
}