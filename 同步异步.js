const { rejects } = require("assert")
const { resolve } = require("path/posix")

//同期関数
const tongqi=()=>{
    console.log("aaaaaaa")
    return'完了'
}
const result= tongqi()
console.log('関数の返却値:'+result)



//非同期関数
const getName = ()=>{
    const url = 'https://api.github.com/users/deatiger'
    fetch(url).then(res=> res.json())
    .then(json=>{
        console.log('成功')
        return json.login
    }).catch(error=>{
      console.error('エラー')
        return null
    })
}
const message='name is'
const username=getName()
console.log(message+username)


//解决先输出message,username未定义问题
const getName = ()=>{
    return new Promise((resolve,reject)=>{

    
    const url = 'https://api.github.com/users/deatiger'
    fetch(url).then(res=> res.json())
    .then(json=>{
        console.log('成功')
        return resolve(json.login) 
    }).catch(error=>{
      console.error('エラー')
        return  reject(null)
    })
})
}
const message='name is'
getName().then(username=>{
    console.log(message+username)
})

//不用promise
const getGitUsername = async()=>{
    const message = 'gitのユーザーIDは';
    const url='https://api.github.com/users/deatiger'
    const json= await fetch(url)
    .then(res=>{
        console.log('成功')
        return res.json()
    }).catch(error=>{
        console.error('失敗',error)
        return null
    });
    console.log(message+json.login)
}
    getGitUsername()
